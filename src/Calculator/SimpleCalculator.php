<?php

namespace App\Calculator;

use App\Exception\CalculatorException;

class SimpleCalculator
{
    public function add($var1, $var2)
    {
        $this->checkParameter($var1);
        $this->checkParameter($var2);

        return $var1 + $var2;
    }

    /**
     * @param $var1
     */
    public function checkParameter($var1): void
    {
        if (!is_numeric($var1)) {
            throw new CalculatorException($var1.' is not numeric');
        }
    }
}
