<?php

namespace App\Tests\Calculator;

use App\Calculator\SimpleCalculator;
use App\Exception\CalculatorException;
use PHPUnit\Framework\TestCase;

class SimpleCalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new SimpleCalculator();
        $result = $calculator->add(12, 15);
        $this->assertEquals(27, $result);

        $result = $calculator->add(-12, 15);
        $this->assertEquals(3, $result);
    }

    public function testNonNumericValueThrowsException()
    {
        $calculator = new SimpleCalculator();
        $this->expectException(CalculatorException::class);
        $calculator->add(1, 'a');
    }

    public function testNonNumericValue2ThrowsException()
    {
        $calculator = new SimpleCalculator();
        $this->expectException(CalculatorException::class);
        $calculator->add('a', 1);
    }
}
